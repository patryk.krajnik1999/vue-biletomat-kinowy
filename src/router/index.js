import { createRouter, createWebHistory } from 'vue-router'
import ScreeningsList from "@/pages/ScreeningsList.vue";
import ScreeningDetails from "@/pages/ScreeningDetails.vue";
import NotFound from "@/pages/NotFound.vue";

const routes = [
  {
    path: '/',
    redirect: '/screenings',
  },
  {
    path: '/screenings',
    component: ScreeningsList,
  },
  {
    path: '/screenings/:id',
    component: ScreeningDetails,
    props: true,
  },
  {
    path: '/:notFound(.*)',
    component: NotFound,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
