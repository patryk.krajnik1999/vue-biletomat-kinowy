export default {
  async loadData(context) {
    const response = await fetch(`http://biletomat_kinowy/screenings`);

    if (!response.ok) {
      throw new Error("Nie udało się pobrać danych.");
    }

    const responseData = await response.json();

    const screenings = [];
    for (const res in responseData) {
      const screening = {
        id: "s" + res,
        uuId: responseData[res]["id"],
        dateTime: responseData[res]["dateTime"]["date"],
        movieTitle: responseData[res]["movieTitle"],
        movieDescription: responseData[res]["movieDescription"],
        movieCategories: responseData[res]["movieCategories"],
        movieDuration: responseData[res]["movieDuration"],
        movieAccessAge: responseData[res]["movieAccessAge"],
        hallNumber: responseData[res]["hallNumber"],
        hallColumns: responseData[res]["hallColumns"],
        hallRows: responseData[res]["hallRows"],
        seats: responseData[res]["seats"],
      };
      // console.log(screening);
      // console.log(work["submits"].some(w => w.userId === 3));
      screenings.push(screening);
    }

    context.commit("setScreenings", screenings);
  },

  async takeSeats(context, payload) {
    const data = {
      screeningId: payload.screeningId,
      seatsNumber: payload.seats.length,
      seats: payload.seats,
    };

    const response = await fetch(`http://biletomat_kinowy/takeSeats`, {
      method: "POST",
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    await context.commit("takeSeats", {
      screeningId: payload.screeningId,
      seatsNumber: payload.seats.length,
      seats: payload.seats,
    });

    setTimeout(async function () {
      const response = await fetch(`http://biletomat_kinowy/checkSeats`, {
        method: "POST",
        body: JSON.stringify(data),
      });
      const responseData = await response.json();
      if (responseData == null) {
        context.commit("giveSeats", {
          screeningId: payload.screeningId,
          seatsNumber: payload.seats.length,
          seats: payload.seats,
        });
      }
    }, 60000);

    console.log(responseData);
  },

  async pay(context, payload) {
    const data = {
      cost: payload.cost,
      seats: payload.seats,
      screeningId: payload.screeningId,
    };

    const response = await fetch(`http://biletomat_kinowy/pay`, {
      method: "POST",
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    console.log(responseData);
  },
};
