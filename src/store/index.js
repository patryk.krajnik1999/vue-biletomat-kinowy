import { createStore } from 'vuex';
import actions from "@/store/actions";
import mutations from "@/store/mutations";
import getters from "@/store/getters";
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

export default createStore({
  state: {
    screenings: [],
  },
  actions,
  mutations,
  getters,
  plugins: [vuexLocal.plugin]
})
