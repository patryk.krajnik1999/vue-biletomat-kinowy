export default {
  setScreenings(state, payload) {
    state.screenings = payload;
  },
  takeSeats(state, payload) {
    for (let i = 0; i < payload["seatsNumber"]; i++) {
      state.screenings.find(
        (screening) => screening.uuId === payload["screeningId"]
      ).seats[payload["seats"][i]] = 1;
    }
  },
  giveSeats(state, payload) {
    for (let i = 0; i < payload["seatsNumber"]; i++) {
      state.screenings.find(
        (screening) => screening.uuId === payload["screeningId"]
      ).seats[payload["seats"][i]] = 0;
    }
  },
};
